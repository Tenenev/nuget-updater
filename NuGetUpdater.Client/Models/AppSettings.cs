﻿using Newtonsoft.Json;

namespace NuGetUpdater.Client.Models
{
    public class AppSettings
    {
        [JsonProperty("packageSources")]
        public string Sources { get; set; }
    }
}
