﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAPICodePack.Dialogs;
using NuGetUpdater.Client.Annotations;
using NuGetUpdater.Client.Common;
using NuGetUpdater.Core.Models;
using NuGetUpdater.Core.Services;

namespace NuGetUpdater.Client.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private string _solutionDirectory;

        public string SolutionDirectory
        {
            get => _solutionDirectory;
            set
            {
                _solutionDirectory = value;
                OnPropertyChanged(nameof(SolutionDirectory));
            }
        }

        public ObservableCollection<string> Packages { get; } = new();

        public ObservableCollection<NuGetPackageInfo> FoundPackages { get; } = new();

        private string _selectedPackage;

        public string SelectedPackage
        {
            get => _selectedPackage;
            set
            {
                _selectedPackage = value;
                OnPropertyChanged(nameof(SelectedPackage));

                Application.Current.Dispatcher.InvokeAsync(async () => await ProcessSolutionAsync(SolutionDirectory));
            }
        }

        private ICommand _selectSolutionPathCommand;
        public ICommand SelectSolutionPathCommand
        {
            get
            {
                return _selectSolutionPathCommand ??
                       (_selectSolutionPathCommand = new RelayCommand(obj =>
                       {
                           var dialog = new CommonOpenFileDialog();
                           dialog.IsFolderPicker = true;
                           var result = dialog.ShowDialog();

                           if (result != CommonFileDialogResult.Ok) return;

                           if (!Directory.Exists(dialog.FileName))
                           {
                               MessageBox.Show("Directory is not exists");
                               return;
                           }

                           Application.Current.Dispatcher.InvokeAsync(async () => await ProcessSolutionAsync(dialog.FileName));
                       }));
            }
        }

        private readonly NuGetRepositoryService nuGetRepositoryService;
        private readonly LocalPackageService localPackageService;

        private readonly string[] _packageSources;

        public MainViewModel()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appSettings.json").Build();

            var packagesSection = config.GetSection("packages");
            var packages = packagesSection.Get<string[]>();
            foreach (var package in packages)
            {
                Packages.Add(package);
            }

            var packageSourcesSection = config.GetSection("packageSources");
            _packageSources = packageSourcesSection.Get<string[]>();

            var solutionDirSection = config.GetSection("defaultSolutionDir");
            SolutionDirectory = solutionDirSection.Get<string>();

            nuGetRepositoryService = new NuGetRepositoryService(_packageSources);
            localPackageService = new LocalPackageService(SolutionDirectory, nuGetRepositoryService);
        }

        public void Loaded()
        {
            SelectedPackage = Packages.FirstOrDefault();
        }

        private async Task ProcessSolutionAsync(string solutionPath)
        {
            if (solutionPath == null || SelectedPackage == null) return;

            foreach (var foundPackage in FoundPackages.ToList())
            {
                FoundPackages.Remove(foundPackage);
            }

            var foundPackages = await localPackageService.ScanPackagesAsync(SelectedPackage);

            foreach (var foundPackage in foundPackages)
            {
                FoundPackages.Add(foundPackage);
            }
        }        

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
