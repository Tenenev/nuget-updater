﻿using System.Windows;
using NuGetUpdater.Client.ViewModels;

namespace NuGetUpdater.Client.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = new MainViewModel();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            ((MainViewModel) DataContext).Loaded();
        }
    }
}
