﻿using NuGet.Versioning;
using NuGetUpdater.Core.Models;

namespace NuGetUpdater.Core.Extensions
{
    public static class NuGetVersionExtensions
    {
        public static NuGetVersionInfo ToModel(this NuGetVersion version)
        {
            return new NuGetVersionInfo
            {
                Version = version.Version.ToString(),
                IsLegacy = version.IsLegacyVersion
            };
        }
    }
}
