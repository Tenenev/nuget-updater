﻿using System.Linq;

namespace NuGetUpdater.Core.Models
{
    public class NuGetPackageInfo
    {
        public string Id { get; set; }

        public NuGetVersionInfo Version { get; set; }

        public string DisplayVersion => Version?.ToString();
        public string Path { get; set; }

        public NuGetVersionInfo[] AvaliableVersions { get; set; }

        public NuGetVersionInfo LatestVersion => AvaliableVersions?.OrderByDescending(v => v).FirstOrDefault();

        public bool HasUpdate
        {
            get
            {
                if (Version == null || AvaliableVersions == null) return false;

                return AvaliableVersions.Any(v => v > Version);
            }
        }
    }
}
