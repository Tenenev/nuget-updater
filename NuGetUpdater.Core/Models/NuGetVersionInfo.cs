﻿using System;

namespace NuGetUpdater.Core.Models
{
    public class NuGetVersionInfo : IComparable, IComparable<NuGetVersionInfo>, IEquatable<NuGetVersionInfo>
    {
        public string Version { get; set; }

        public bool IsLegacy { get; set; }

        public int CompareTo(NuGetVersionInfo other)
        {
            if (other == null)
            {
                throw new Exception("Cannot to compare two objects");
            }

            return this.Version.CompareTo(other.Version);
        }

        public int CompareTo(object obj)
        {
            var other = obj as NuGetVersionInfo;

            if(other == null)
            {
                throw new Exception("Cannot to compare two objects");
            }

            return this.Version.CompareTo(other.Version);
        }

        public bool Equals(NuGetVersionInfo other)
        {
            if(other == null)
            {
                return false;
            }

            return this.Version == other.Version;
        }

        public static bool operator <(NuGetVersionInfo left, NuGetVersionInfo right)
        {
            return left.CompareTo(right) == -1;
        }

        public static bool operator >(NuGetVersionInfo left, NuGetVersionInfo right)
        {
            return left.CompareTo(right) == 1;
        }

        public override string ToString()
        {
            return Version.ToString();
        }
    }
}
