﻿using NuGet.Packaging;
using NuGetUpdater.Core.Extensions;
using NuGetUpdater.Core.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace NuGetUpdater.Core.Services
{
    public class LocalPackageService
    {
        private readonly string _baseDirectory;
        private readonly NuGetRepositoryService nuGetRepositoryService;

        public LocalPackageService(string baseDirectory, NuGetRepositoryService nuGetRepositoryService)
        {
            this._baseDirectory = baseDirectory;
            this.nuGetRepositoryService = nuGetRepositoryService;
        }

        public async Task<List<NuGetPackageInfo>> ScanPackagesAsync(string packageName)
        {
            var foundPackages = new List<NuGetPackageInfo>();

            if (packageName == null) return foundPackages;

            var packageStores = await Task.Factory.StartNew(() => Directory.GetFiles(_baseDirectory, "packages.config", SearchOption.AllDirectories));
            var projectFiles = await Task.Factory.StartNew(() => Directory.GetFiles(_baseDirectory, "*.csproj", SearchOption.AllDirectories));

            foreach (var packageStore in packageStores)
            {
                var packages = await ProcessPackageStoreAsync(packageStore);

                var foundPackage = packages.FirstOrDefault(p => p.PackageIdentity.Id == packageName);

                if (foundPackage == null) continue;

                foundPackages.Add(new NuGetPackageInfo
                {
                    Id = foundPackage.PackageIdentity.Id,
                    Version = foundPackage.PackageIdentity.Version.ToModel(),
                    Path = Path.GetDirectoryName(packageStore),
                    AvaliableVersions = await nuGetRepositoryService.GetPackageVersionsAsync(foundPackage.PackageIdentity.Id)
                });
            }

            //var packageSourceProvider = new PackageSourceProvider(new NullSettings());
            //var sourceRepositoryProvider = new SourceRepositoryProvider(packageSourceProvider, providers);
            //var localPackageCachePath = Path.Combine(NuGetEnvironment.GetFolderPath(NuGetFolderPath.NuGetHome), "packages");
            //var localNuGetPackageManager = new NuGetPackageManager(sourceRepositoryProvider, new NullSettings(), localPackageCachePath)
            //{
            //    PackagesFolderNuGetProject = new FolderNuGetProject(localPackageCachePath),
            //};

            return foundPackages;
        }

        private async Task<List<PackageReference>> ProcessPackageStoreAsync(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException($"{filePath} file is not found");
            }

            await using var fs = File.OpenRead(filePath);
            var doc = await XDocument.LoadAsync(fs, LoadOptions.None, CancellationToken.None);

            var reader = new PackagesConfigReader(doc);
            return reader.GetPackages().ToList();
        }
    }
}
