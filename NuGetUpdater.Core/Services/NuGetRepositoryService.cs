﻿using NuGet.Common;
using NuGet.Configuration;
using NuGet.Protocol.Core.Types;
using NuGet.Versioning;
using NuGetUpdater.Core.Extensions;
using NuGetUpdater.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NuGetUpdater.Core.Services
{
    public class NuGetRepositoryService
    {
        private readonly string[] _packageSources;

        private List<Lazy<INuGetResourceProvider>> providers = new(Repository.Provider.GetCoreV3());

        public NuGetRepositoryService(string[] packageSources)
        {
            this._packageSources = packageSources;
        }

        public async Task<NuGetVersionInfo[]> GetPackageVersionsAsync(string packageId)
        {
            var packageVersions = new List<NuGetVersion>();

            foreach (var packageSource in _packageSources)
            {
                var source = new PackageSource(packageSource);
                var remoteNuGetFeed = new SourceRepository(source, providers);

                var findVersionResource = await remoteNuGetFeed.GetResourceAsync<FindPackageByIdResource>();
                var versions = await findVersionResource.GetAllVersionsAsync(packageId, new NullSourceCacheContext(),
                    new NullLogger(), CancellationToken.None);

                packageVersions.AddRange(versions);
            }

            return packageVersions.Distinct().Select(x => x.ToModel()).OrderByDescending(x => x).ToArray();
        }

        private async Task<IDictionary<string, List<NuGetVersion>>> SearchPackagesAsync(string query)
        {
            var totalResults = new Dictionary<string, List<NuGetVersion>>();

            foreach (var packageSource in _packageSources)
            {
                var source = new PackageSource(packageSource);
                var remoteNuGetFeed = new SourceRepository(source, providers);

                var searchResource = await remoteNuGetFeed.GetResourceAsync<PackageSearchResource>();
                if (searchResource != null)
                {
                    var searchFilter = new SearchFilter(true) { OrderBy = SearchOrderBy.Id, IncludeDelisted = false };
                    foreach (var result in await searchResource.SearchAsync(query, searchFilter, 0, 30, NullLogger.Instance, CancellationToken.None))
                        totalResults.Add(result.Identity.Id, new List<NuGetVersion>((await result.GetVersionsAsync()).Select(vi => vi.Version)));
                }
                else
                {
                    var listResource = await remoteNuGetFeed.GetResourceAsync<ListResource>();
                    var allPackages = await listResource.ListAsync(query, true, true, false, NullLogger.Instance, CancellationToken.None);
                    var enumerator = allPackages.GetEnumeratorAsync();
                    var searchResults = new List<IPackageSearchMetadata>();
                    while (true)
                    {
                        var moved = await enumerator.MoveNextAsync();
                        if (!moved) break;
                        if (enumerator.Current == null) break;
                        searchResults.Add(enumerator.Current);
                    }
                    foreach (var result in searchResults)
                    {
                        if (!totalResults.ContainsKey(result.Identity.Id))
                            totalResults.Add(result.Identity.Id, new List<NuGetVersion>());
                        totalResults[result.Identity.Id].Add(result.Identity.Version);
                    }
                }
            }

            return totalResults;
        }
    }
}
